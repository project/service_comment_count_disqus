# Service Comment Disqus

The Service Comment Count Disqus module integrates the [Service Comment Count]
(https://www.drupal.org/project/service_comment_count) with the [Disqus]
(https://disqus.com) comment service.

## Features

* Fetches and store the comment number of Disqus threads.

## Installation

1. Download with composer.
2. Enable the module.

## Configuration
1. Configure the module at `/admin/structure/services/service-comment-count`.

## Requirements

* [Service Comment Count](https://www.drupal.org/project/service_comment_count)
* [Disqus](https://www.drupal.org/project/disqus)
